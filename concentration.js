<!-- Original:  Brian Gosselin (bgaudiodr@aol.com) -->
<!-- Web Site:  http://scriptasylum.com/ (old http://www.bgaudiodr.iwarp.com)  -->
<!-- Freeware License: http://scriptasylum.com/faqpage.html#p7 -->
<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

var modulepath;


var map=new Array(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 

16, 17, 17, 18, 18);
var user = new Array();
var temparray = new Array();
var clickarray = new Array(0, 0);
var ticker, sec, min, ctr, id, oktoclick, finished;
var messageagain="Match the pictures as fast as you can!";
var less60="Fluke! Bet you cant get less than 60 seconds again.";
var less120="Wow, you are becoming a master! ";
var less180="Excellent. Bet you cant get better than two minutes!";
var more180="The less said the better.";
var iconpath;
var iconwidth;
var iconheight;
var besttime=-1;

function loadpics()
{
  var pics = new Array();
  for (i = 0; i <= 18; i++) {
    if (iconwidth&&iconwidth!=''&&iconheight&&iconheight!='')
      pics[i] = new Image(iconwidth, iconheight);
    else
      pics[i] = new Image();

    pics[i].src = iconpath + i + '.jpg';
  }
}

function init()
{
  clearTimeout(id);
  for (i = 0; i <= 35 ;i++) {
   user[i] = 0;
  }
  ticker = 0;
  min = 0;
  sec = 0;
  ctr = 0;
  oktoclick = true;
  finished = 0;
  document.getElementById('concentration_counter').innerHTML = "";
  document.getElementById('concentration_formmessage').innerHTML=messageagain;
  document.getElementById('concentration_startstop').value="Reset";
  scramble();
  runclk();
  for (i = 0; i <= 35; i++) {
    document.f[('img'+i)].src = iconpath+"0.jpg";
  }
}

function runclk()
{
  min = Math.floor(ticker/60);
  sec = (ticker-(min*60))+'';
  if(sec.length == 1)
  {
    sec = "0"+sec
  };
  ticker++;
  document.getElementById('concentration_counter').innerHTML = min+":"+sec;
  if (document.getElementById('concentration_startstop').value=="Reset")
    id = setTimeout('runclk()', 1000);
}

function scramble()
{
  for (z = 0; z < 5; z++) {
    for (x = 0; x <= 35; x++) {
temparray[0] = Math.floor(Math.random()*36);
temparray[1] = map[temparray[0]];
temparray[2] = map[x];
map[x] = temparray[1];
map[temparray[0]] = temparray[2];
      }
   }
}
function showimage(but)
{
  if (document.getElementById('concentration_startstop').value=="Start")
    document.getElementById('concentration_startstop').click();

  if (oktoclick) {
oktoclick = false; 
document.f[('img'+but)].src = iconpath+map[but]+'.jpg';
if (ctr == 0) {
ctr++;
clickarray[0] = but;
oktoclick = true;
} else {
clickarray[1] = but;
ctr = 0;
setTimeout('returntoold()', 600);
      }
   }
}

function returntoold()
{
  if ((clickarray[0] == clickarray[1]) && (!user[clickarray[0]]))
  {
    document.f[('img'+clickarray[0])].src = iconpath+"0.jpg";
    oktoclick = true;
  }
  else
  {
if (map[clickarray[0]] != map[clickarray[1]]) {
if (user[clickarray[0]] == 0) {
document.f[('img'+clickarray[0])].src = iconpath+"0.jpg";
}
if (user[clickarray[1]] == 0) {
document.f[('img'+clickarray[1])].src = iconpath+"0.jpg";
   }
}
if (map[clickarray[0]] == map[clickarray[1]]) {
if (user[clickarray[0]] == 0&&user[clickarray[1]] == 0) { finished++; }
user[clickarray[0]] = 1;
user[clickarray[1]] = 1;
}
if (finished >= 18)
{
  if (ticker<60)
    document.getElementById('concentration_formmessage').innerHTML=less60;
  else if (ticker<120)
    document.getElementById('concentration_formmessage').innerHTML=less120;
  else if (ticker<180)
    document.getElementById('concentration_formmessage').innerHTML=less180;
  else 
    document.getElementById('concentration_formmessage').innerHTML=more180;

  document.getElementById('concentration_startstop').value="Start";


  if (document.getElementById('concentration_winform')&&(besttime==-1||ticker<besttime))
  {
    document.getElementById('concentration_message').value=document.getElementById('concentration_formmessage').innerHTML;
    document.getElementById('concentration_ticker').value=ticker;
    document.getElementById('concentration_winform').submit();
  }

}
else
{
  oktoclick = true;
}
   }
}

function displaytable()
{
  for (r = 0; r <= 5; r++) {
    document.write('<tr>');
    for (c = 0; c <= 5; c++) {
      document.write('<td style="padding:0px; font-size:3em;">');
      document.write('<a href="javascript:showimage('+((6*r)+c)+')"   onClick="document.getElementById(\'concentration_startstop\').focus()"/>');
    if (iconwidth&&iconwidth!=''&&iconheight&&iconheight!='')
      document.write('<img width="'+iconwidth+'" height="'+iconheight+'" src="'+iconpath+'0.jpg" name="img'+((6*r)+c)+'" border="0"/>');
   else
      document.write('<img  src="'+iconpath+'0.jpg" name="img'+((6*r)+c)+'" border="0"/>');

      document.write('</a></td>');
    }
    document.write('</tr>');
  }
}