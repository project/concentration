$ Id: $

------------
Description
------------

Plays the memory game concentration where users match pictures as quickly as possible.
This javascript (original javascript script by Brian Gosselin) based game is a block
where the pictures can be themed to your site. The user that solves it fastest has his
name saved. The games win messages are configurable in the configuration settings.
Also the URL of the icons and the size of the icons can be set in the configuration
settings.

------------
Requirements
------------

- Drupal 5

- Concentration module should be enabled

------------
Installation
------------

- Create a new directory "concentration" in your "modules" directory and place the
  entire contents of this concentration directory in it.

- Enable the module by navigating to Administer -> Site building -> Modules.

- Grant the proper access to user accounts under Administer -> User management
  -> Access control. The most important setting is "play concentration" for
  all roles.

- Enable the Concentration block by navigating to Administer -> Site building ->
  Blocks.

------------
Credits
------------
Written by
  - Glenn Linde <glinde AT skinhat DOT com>

Based upon javascript script by
  - Brian Gosselin   Freeware License: http://scriptasylum.com/faqpage.html#p7 
